import { useState, useEffect } from "react";
import './App.css';

function App() {
    const [albumId, setAlbumId] = useState(0);
    const [albums, setAlbums] = useState([]);

    const [photos, setPhotos] = useState([])

    const onClick = (albumId) => (e) => {
        setAlbumId(albumId);

        fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${albumId}`)
            .then((response) => response.json())
            .then((data) => {
                setPhotos(data)
            })
    }

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/albums")
            .then((response) => response.json())
            .then((data) => {
                setAlbums(data);
            });
    });

    return (
        <div className="gallery">
            <aside className="albums">
                <h4 className="albums__title">Albums</h4>
                <ul className="albums-list">
                    {albums.map(({ id, title }) => {
                        let classes = "albums-list__item";

                        if (albumId === id) {
                            classes += " albums-list__item--active";
                        }

                        return (
                            <li key={id} className={classes} onClick={onClick(id)}>{title}</li>
                        );
                    })}
                </ul>
            </aside>
            <main className="photos">
                {photos.map(({id, title, url}) => {
                    return (
                        <>
                            <div className="photos__title">{title}</div>
                            <img key={id} src={url}></img>
                        </>
                    );
                })}
            </main>
        </div>
    );
}

export default App;
